import { User } from './user'

export class Database {

    private users: User[] = [];
  
    constructor() {
        this.addUser(new User('User_i01@dummy.likeyaa.com',	true, 'snUserName-1x'));
        this.addUser(new User('User_i02@dummy.likeyaa.com',	true, 'snUserName-2x'));
        this.addUser(new User('User_i03@dummy.likeyaa.com',	true, null,	'snUserName-2x'));
        this.addUser(new User('User_i04@dummy.likeyaa.com',	true, 'snUserName-3x'));
        this.addUser(new User('User_i05@dummy.likeyaa.com',	true, null,	'snUserName-3x'));
        this.addUser(new User('User_i06@dummy.likeyaa.com',	true, null, null,	'snUserName-3x'));
        this.addUser(new User('User_i07@dummy.likeyaa.com',	true, 'snUserName-Dbl'));
        this.addUser(new User('User_i08@dummy.likeyaa.com',	true, 'snUserName-Dbl'));
        this.addUser(new User('User_i09@dummy.likeyaa.com',	true, 'snUserName-Share'));
        this.addUser(new User('User_i10@dummy.likeyaa.com',	true, 'snUserName-Share', 'snUserName-Share', 'snUserName-Share'));
        this.addUser(new User('User_i11@dummy.likeyaa.com',	true, 'snUserName-Stat'));
        this.addUser(new User('User_i12@dummy.likeyaa.com',	false, 'snUserName-Stat'));
        this.addUser(new User('User_i13@dummy.likeyaa.com',	false, 'snUserName-Corp'));
        this.addUser(new User('User_c01@dummy.likeyaa.com',	true, null,	'snUserName-Corp'));
    }
    
    addUser(user: User) {
      this.users.push(user);
    }
  
    getUsersByFilter(callbackfn): User[] {
        return this.users.filter(callbackfn)
    }

    dump() {
      console.log(this.users)
    }
  
  }
  