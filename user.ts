export class User {
    
    mailAddress: string;

    igName?: string = null;
    fbName?: string = null;
    ytName?: string = null;

    isActive: boolean = false;
    
    constructor(mailAddress: string, isActive: boolean, 
      igName: string = null, fbName: string = null, ytName: string = null) {
        this.mailAddress = mailAddress;
        this.isActive = isActive;
        this.igName = igName;
        this.fbName = fbName;
        this.ytName = ytName;
    }
}
