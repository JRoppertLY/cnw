import { expect, assert } from "chai";
import * as chai from "chai";
import fetch from "node-fetch";
import { suite, test } from "mocha-typescript";
import * as chaiDeepMatch from "chai-deep-match"; // equal to => var chaiDeepMatch = require('chai-deep-match');

import { getConcurrentSocialNetworks } from '../index'

chai.use(chaiDeepMatch);

@suite class getConcurrentNetworks {

    @test async 'should not expose whether an influencer is registered with likeyaa or not - unregistered user'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-NonExisting');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(0);
    }

    @test async 'should not expose whether an influencer is registered with likeyaa or not - registered user'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-1x');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(0);
    }

    @test async 'should not indicate concurrent network use if a social network username is not shared by multiple users.'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-1x');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(0);
    }

    @test async 'should return 2 concurrent networks if a social network username is shared by active users in 2 different social networks.'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-2x');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(2);
    }

    @test async 'should return 3 concurrent networks if a social network username is shared by active users in 3 different social networks.'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-3x');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(3);
    }

    @test async 'should not indicate concurrent network use if a social network username is shared by an active and an inactive user.'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('smUserName=snUserName-Stat');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(0);
    }

    @test async 'should not indicate concurrent network use if a social network username is shared by an influencer and a company.'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-Corp');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(0);
    }

    @test async 'should not indicate concurrent network use if a social network username is used by different people for the same social network (Data Integrity Problem).'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-Dbl');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(0);
    }

    @test async 'should return 3 concurrent network if a social network username is used by one user for all 3 social networks and by another user for one of them (Data Integrity Problem).'() {
        const concurrentNetworks: string[] = getConcurrentSocialNetworks('snUserName-Share');
        expect(concurrentNetworks, 'concurrentNetworks').to.not.equal(null);
        expect(concurrentNetworks.length, 'concurrentNetworks.length').to.equal(3);
    }
}
